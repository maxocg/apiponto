﻿using Apipontos.Context;
using Apipontos.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Apipontos.Controllers
{
    [RoutePrefix("api/ponto" )]
    public class DefaultController : ApiController
    {
        [HttpGet]
        [Route("todos")]
        public HttpResponseMessage GetPontos()
        {
            try
            {
                using (var context = new Contexto())
                {
                    var pontos = context.Pontos.ToList();
                    return Request.CreateResponse(HttpStatusCode.OK, pontos.ToArray());
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("buscaid/{id:int}")]
        public HttpResponseMessage GetByID(int id)
        {
            try
            {
                using (var context = new Contexto())
                {
                    var ponto = context.Pontos.FirstOrDefault(c=>c.Id==id);

                    if (ponto==null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Cliente não encontrado!");
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, ponto);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("insert")]
        public HttpResponseMessage PostPonto(Ponto ponto)
        {
            try
            {
                using (var context = new Contexto())
                {
                    context.Pontos.Add(ponto);
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "sucesso");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut]
        [Route("update")]
        public HttpResponseMessage UpdatePonto(Ponto ponto)
        {
            try
            {
                using (var context = new Contexto())
                {
                    context.Pontos.Attach(ponto);
                    context.Entry(ponto).State = System.Data.Entity.EntityState.Modified;    
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "sucess");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete]
        [Route("delete/{id:int}")]
        public HttpResponseMessage DeletePonto(int id)
        {
            try
            {
                using (var context = new Contexto())
                {
                    var pontoNoBanco = context.Pontos.FirstOrDefault( c=>c.Id==id);
                    context.Pontos.Remove(pontoNoBanco);
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "sucess");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }


    }
}
