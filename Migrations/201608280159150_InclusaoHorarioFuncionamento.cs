namespace Apipontos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InclusaoHorarioFuncionamento : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pontoes", "HorarioFuncionamento", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pontoes", "HorarioFuncionamento");
        }
    }
}
