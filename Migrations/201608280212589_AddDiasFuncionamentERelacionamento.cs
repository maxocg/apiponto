namespace Apipontos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDiasFuncionamentERelacionamento : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DiaFuncionamentoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DiaDaSemana = c.Int(nullable: false),
                        HorarioFuncionamento = c.DateTime(),
                        IdPonto = c.Int(nullable: false),
                        Ponto_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pontoes", t => t.Ponto_Id, cascadeDelete: true)
                .Index(t => t.Ponto_Id);
            
            DropColumn("dbo.Pontoes", "HorarioFuncionamento");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pontoes", "HorarioFuncionamento", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.DiaFuncionamentoes", "Ponto_Id", "dbo.Pontoes");
            DropIndex("dbo.DiaFuncionamentoes", new[] { "Ponto_Id" });
            DropTable("dbo.DiaFuncionamentoes");
        }
    }
}
