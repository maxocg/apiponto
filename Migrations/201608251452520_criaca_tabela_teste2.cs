namespace Apipontos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class criaca_tabela_teste2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Testes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Descricao = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Testes");
        }
    }
}
