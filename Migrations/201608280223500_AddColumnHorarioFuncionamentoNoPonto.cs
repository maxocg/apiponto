namespace Apipontos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnHorarioFuncionamentoNoPonto : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DiaFuncionamentoes", "Ponto_Id", "dbo.Pontoes");
            DropIndex("dbo.DiaFuncionamentoes", new[] { "Ponto_Id" });
            AddColumn("dbo.Pontoes", "HorarioFuncionamento", c => c.DateTime(nullable: false));
            DropTable("dbo.DiaFuncionamentoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DiaFuncionamentoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DiaDaSemana = c.Int(nullable: false),
                        HorarioFuncionamento = c.DateTime(),
                        IdPonto = c.Int(nullable: false),
                        Ponto_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Pontoes", "HorarioFuncionamento");
            CreateIndex("dbo.DiaFuncionamentoes", "Ponto_Id");
            AddForeignKey("dbo.DiaFuncionamentoes", "Ponto_Id", "dbo.Pontoes", "Id", cascadeDelete: true);
        }
    }
}
