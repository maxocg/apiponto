namespace Apipontos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnHorarioFuncionamentoNoPontoV3 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Pontoes", "HorarioFuncionamento");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pontoes", "HorarioFuncionamento", c => c.DateTime(nullable: false));
        }
    }
}
