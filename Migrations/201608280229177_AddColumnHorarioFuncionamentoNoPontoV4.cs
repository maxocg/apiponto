namespace Apipontos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnHorarioFuncionamentoNoPontoV4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pontoes", "Horario", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pontoes", "Horario");
        }
    }
}
