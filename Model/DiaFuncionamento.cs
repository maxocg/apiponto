﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apipontos.Model
{
    public class DiaFuncionamento
    {
        public int Id { get; set; }
        public DiaDaSemana DiaDaSemana { get; set; }
        public DateTime? HorarioFuncionamento { get; set; }
        public int IdPonto { get; set; }
        public virtual Ponto Ponto { get; set; }
    }
    public enum DiaDaSemana
    {
        SEGUNDA,
        TERCA,
        QUARTA,
        QUINTA,
        SEXTA,
        SABADO,
        DOMINGO
    }
}