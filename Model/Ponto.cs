﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apipontos.Model
{
    public class Ponto
    {
        public int Id { get; set; }
        public String Nome { get; set; }
        public String Descricao { get; set; }
        public String Endereco { get; set; }
        public String Latitude { get; set; }
        public String Longitude { get; set; }
        public String UrlImagem { get; set; }
        public String Horario { get; set; }
        public decimal Valor { get; set; }
        
    }
}